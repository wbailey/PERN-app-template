import { Request, Response } from "express";
import passport from "passport";
import { AbstractRouter } from "@/server/lib";

export default class AuthRoutes extends AbstractRouter {
	constructor() {
		super();
		this._baseUrl = "/Oauth2.0";
		this._registerRoutes();
	}

	protected _registerRoutes(): void {
		this._router.get("/", (_req: Request, res: Response): void => {
			res.send("hello!");
		});
		this._router
			.route("/google")
			.all(this._checkRequestMethod("GET"))
			.get((req: Request, res: Response): void => {
				// if jwt is supplied send user id as state
				passport.authenticate("google", {
					scope: ["profile", "email"],
					prompt: "select_account",
					state: "dingus",
				})(req, res);
			});

		this._router
			.route("/google/callback")
			.all(this._checkRequestMethod("GET"))
			.get(
				passport.authenticate("google", { session: false }),
				(_req: Request, res: Response): void => {
					// create JWT and sign it with user data

					// respond with JWT to sign user in
					console.log("req.user", _req.user);
					res.send("hello from the callback!");
				}
			);

		this._router
			.route("/logout")
			.all(this._checkRequestMethod("GET"))
			.get((req: Request, res: Response): void => {
				req.logout((): void => {
					res.json({
						message: "Logout Success.",
					});
				});
			});
	}
}
