import IndexRoutes from "./IndexRoutes";
import AuthRoutes from "./AuthRoutes";

export const indexRouter = new IndexRoutes();
export const authRouter = new AuthRoutes();
