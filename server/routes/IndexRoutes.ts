import { Request, Response } from "express";
import { AbstractRouter } from "@/server/lib";

export default class IndexRoutes extends AbstractRouter {
	constructor() {
		super();
		this._baseUrl = "/";
		this._registerRoutes();
	}

	protected _registerRoutes() {
		this._router
			.route("/")
			.all(this._checkRequestMethod("GET"))
			.get((_req: Request, res: Response): void => {
				res.json({
					message: "welcome to the server",
				});
			});
	}
}
