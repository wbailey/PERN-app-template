// required packages
import "./paths";
import "@/server/config";
import express, { Express, Request, Response, NextFunction } from "express";
import cors from "cors";
import passport from "passport";
import * as routes from "@/server/routes";
import { httpErrors } from "@/server/lib";
import { HttpError } from "@/types";

export const app: Express = express();
const PORT = process.env.PORT || 3000;
// const CLIENT_DEV_PORT = process.env.CLIENT_DEV_PORT || 5173;

// @todo enable only in dev env
app.use(
	cors({
		origin: "*",
	})
);
app.use(passport.initialize());

for (const route of Object.values(routes)) {
	console.log(route.baseUrl);
	app.use(route.baseUrl, route.router);
}

app.all("/*", (_req: Request, _res: Response, next: NextFunction): void =>
	next(new httpErrors.ContentNotFound())
);

app.use(
	(
		error: HttpError,
		_req: Request,
		res: Response,
		_next: NextFunction
	): void => {
		console.log(error);
		res.status(error.responseStatusCode || 500).json({
			message: error.message || "Something went wrong.",
		});
	}
);

app.listen(PORT, (): void => {
	console.log(`Listening on PORT ${PORT}`);
});
