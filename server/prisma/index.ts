import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

const shutdown = async (): Promise<void> => {
	await prisma.$disconnect();
	process.exit();
};

process.on("SIGINT", shutdown);
process.on("SIGTERM", shutdown);

export default prisma;
