abstract class AbstractError extends Error {
	public name: string;
	public responseStatusCode: number;

	constructor(message?: string | undefined) {
		super(message ? message : "An Error has occured.");
		this.name = "GenericError";
		this.responseStatusCode = 500;
	}
}

// client errors
class UnauthorizedError extends AbstractError {
	constructor(message?: string | undefined) {
		super(message ? message : "Unauthorized");
		this.name = "UnauthorizedError";
		this.responseStatusCode = 401;
	}
}

class ContentNotFound extends AbstractError {
	constructor(message?: string | undefined) {
		super(message ? message : "Content not found.");
		this.name = "ContentNotFound";
		this.responseStatusCode = 404;
	}
}

class MethodNotAllowed extends AbstractError {
	constructor(message?: string | undefined) {
		super(message ? message : "Method not allowed.");
		this.name = "MethodNotAllowed";
		this.responseStatusCode = 405;
	}
}

// server errors

export default {
	AbstractError,
	UnauthorizedError,
	ContentNotFound,
	MethodNotAllowed,
};
