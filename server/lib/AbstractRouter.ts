import { Request, Response, NextFunction, Router } from "express";
import { httpErrors } from "@/server/lib";

abstract class AbstractRouter {
	protected _router: Router;
	protected _baseUrl: string;

	constructor() {
		this._router = Router();
		this._baseUrl = "";
	}

	public get router(): Router {
		return this._router;
	}

	public get baseUrl(): string {
		return this._baseUrl;
	}

	protected _checkRequestMethod(
		...allowedMethods: string[]
	): (req: Request, _res: Response, next: NextFunction) => void {
		return (req: Request, _res: Response, next: NextFunction): void => {
			const requestMethod = req.method.toUpperCase();
			if (allowedMethods.includes(requestMethod)) {
				next();
			} else {
				next(
					new httpErrors.MethodNotAllowed(
						`Method: ${requestMethod} not allowed`
					)
				);
			}
		};
	}

	protected _registerRoutes(): void {
		console.log("parent class register routes");
	}
}

export default AbstractRouter;
