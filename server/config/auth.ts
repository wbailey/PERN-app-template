import passport from "passport";
import { Request } from "express";
import { OAuth2Strategy, VerifyFunction, Profile } from "passport-google-oauth";
import prisma from "@/server/prisma";
import { LinkedAccount } from "@prisma/client";
import { httpErrors } from "@/server/lib";

const { GOOGLE_CLIENT_ID, GOOGLE_SECRET, GOOGLE_CALLBACK } = process.env;

const options = {
	clientID: GOOGLE_CLIENT_ID as string,
	clientSecret: GOOGLE_SECRET as string,
	callbackURL: GOOGLE_CALLBACK as string,
	passReqToCallback: true,
};

const verify = async (
	req: Request,
	_accessToken: string,
	_refreshToken: string,
	profile: Profile,
	done: VerifyFunction
) => {
	// TODO: an authenticated user should be able to add/link Oauth accounts, alternatively they could just be given a choice on how to log in and it could be linked up here?
	console.log("req.query", req.query);
	const user = await prisma.user.findUnique({
		where: {
			email: profile?.emails ? profile?.emails[0].value : "",
		},
		include: {
			linkedAccounts: true,
		},
	});

	if (!user) {
		const newUser = await prisma.user.create({
			data: {
				email: profile?.emails ? profile?.emails[0].value : "",
			},
		});
		await prisma.linkedAccount.create({
			data: {
				provider: "GOOGLE",
				accountId: profile.id,
				userId: newUser.id,
			},
		});
		return done(null, newUser);
	} else if (
		user.linkedAccounts.find(
			(linkedAccount: LinkedAccount): boolean =>
				linkedAccount.provider === "GOOGLE"
		)
	) {
		return done(null, user);
	} else {
		const linkedAccountProviders: string = user.linkedAccounts.reduce(
			(accounts: string, linkedAccount: LinkedAccount): string => {
				return `${accounts}${accounts ? "/" : ""}${String(
					linkedAccount.provider
				)}`;
			},
			""
		);
		return done(new httpErrors.UnauthorizedError(linkedAccountProviders));
	}
};

passport.use(new OAuth2Strategy(options, verify));
