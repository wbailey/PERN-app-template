import { existsSync } from "fs";
import { config } from "dotenv";

const dotEnvPath = ".env";
if (existsSync(dotEnvPath)) {
	config({ path: dotEnvPath });
} else {
	throw new Error("file not found: .env");
}

import "@/server/config/auth";
