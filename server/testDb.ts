import {  PrismaClient, User } from "@prisma/client";

const prisma = new PrismaClient();

// https://www.prisma.io/docs/orm/prisma-client/queries/crud
async function main(): Promise<void> {
	try {
		// CREATE
		const newUser: User = await prisma.user.create({
			data: {
				firstName: "Banana",
				lastName: "Brain",
			},
		});
		console.log("new user", newUser);

		const newUserWithLinkedAccount: User = await prisma.user.create({
			data: {
				email: "linked@account.com",
				linkedAccounts: {
					create:{
						provider: "CREDENTIAL",
						password: "abcdefg",
					},
				},
			},
			include: {
				linkedAccounts: true,
			},
		});

		console.log(
			"user created with linked account:",
			newUserWithLinkedAccount
		);

		// READ
		const foundUser: User | null = await prisma.user.findUnique({
			where: {
				id: newUser.id,
			},
		});

		console.log("found user", foundUser);

		const foundUserIncludeLinkedAccounts: User | null =
			await prisma.user.findUnique({
				where: {
					id: newUserWithLinkedAccount.id,
				},
				include: {
					linkedAccounts: true,
				},
			});

		console.log(
			"found user with linked accounts",
			foundUserIncludeLinkedAccounts
		);
		const foundAllUsers: User[] = await prisma.user.findMany();
		console.log("all users", foundAllUsers);

		// UPDATE
		const updatedUser: User | null = await prisma.user.update({
			where: {
				id: newUser.id,
			},
			data: {
				firstName: "Apple",
				lastName: "Sauce",
			},
		});
		console.log("updated user", updatedUser);

		// DESTROY
		const destroyUser: User | null = await prisma.user.delete({
			where: {
				id: newUser.id,
			},
		});
		console.log("deleted user", destroyUser);
	} catch (error) {
		console.error("error in main", error);
	}
}

main()
	.catch(error => {
		console.log("error in main:", error);
	})
	.finally(async (): Promise<void> => {
		const deleteLinkedAccounts = prisma.linkedAccount.deleteMany({});
		const deleteUsers = prisma.user.deleteMany({});
		await prisma.$transaction([deleteLinkedAccounts, deleteUsers]);
	});
