import { useState, useEffect } from "react";
import hello from "@/moduleTest";
console.log(hello);
import "./App.css";
import api from "./api";

function App() {
	const [message, setMessage] = useState("loading");

	console.log(api.getIndex);

	useEffect(() => {
		api.getIndex()
			.then(response => {
				setMessage(response.data.message);
			})
			.catch(console.warn);
	}, []);

	return (
		<>
			<p>{message}</p>
		</>
	);
}

export default App;
