import axios from "axios";

const { VITE_SERVER_URL } = import.meta.env;

const getIndex = async () => axios.get(VITE_SERVER_URL);

export default {
	VITE_SERVER_URL,
	getIndex,
};
