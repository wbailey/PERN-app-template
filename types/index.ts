// Server Types
import { httpErrors } from "@/server/lib";
export type HttpError = InstanceType<typeof httpErrors.AbstractError>;
